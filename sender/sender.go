package main

import (
	"flag"
	"fmt"
	"log"
	"time"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gitlab.com/piLights/proto"
	"google.golang.org/grpc/metadata"
	"image/color"
)

const clientName = "lollight"

var (
	input       chan color.RGBA
	hostName = flag.String("host", "", "<host[:port]> of pidioder")
	colorSender LighterGRPC.RgbServiceClient
)

func send(c color.RGBA) {
	colorMessage := &LighterGRPC.ColorMessage{
		R: int32(c.R),
		G: int32(c.G),
		B: int32(c.B),
		Onstate: true,
		Opacity: 100,
	}

	ctx := context.Background()
	ctx = metadata.NewContext(
		ctx,
		metadata.Pairs("deviceID", clientName),
	)

	success, error := colorSender.SetColor(ctx, colorMessage)
	if error != nil {
		log.Fatal(error)
	} else {
		log.Println(success)
	}
}

func stepChannel(cur, target uint8) uint8 {
	switch {
	case cur < target:
		return cur + 1
	case cur > target:
		return cur - 1
	default:
		return cur
	}
}

func fadeStep(cur, target color.RGBA) color.RGBA {
	return color.RGBA{
		stepChannel(cur.R, target.R),
		stepChannel(cur.G, target.G),
		stepChannel(cur.B, target.B),
		100,
	}
}

func fader() {
	var current color.RGBA
	var last color.RGBA

	for {
		select {
		case c := <-input:
			current = c
		default:
			last = fadeStep(last, current)
			send(last)
			time.Sleep(10 * time.Millisecond)
		}
	}
}

func main() {
	flag.Parse()

	if *hostName == "" {
		log.Fatal("No PiDioder address given.")
	}

	//Make the colorSender
	connection, error := grpc.Dial(*hostName, grpc.WithInsecure())
	if error != nil {
		log.Fatal(error)
	}
	defer connection.Close()

	colorSender = LighterGRPC.NewRgbServiceClient(connection)

	input = make(chan color.RGBA)

	go fader()

	for {
		var r, g, b int

		_, err := fmt.Scanf("%d %d %d", &r, &g, &b)

		if err != nil {
			log.Fatal(err)
		}

		input <- color.RGBA{uint8(r), uint8(g), uint8(b), 100}
	}
}